# from __future__ import (absolute_import, division,
#                         print_function, unicode_literals)
# from builtins import *

import sys
import time

import cocotb
from cocotb.triggers import Timer
from cocotb.result import TestFailure

from pyflashrom import ffi, lib

# gwkd = weakref.WeakKeyDictionary()
nogc = list()

usec = 1000
clkper = 5000
poweronper = 1500000
# resetper = 300*usec
resetper = 10000

offset = 0x100000
word0 = 0x00000093
word1 = 0x00000193

gdut = None
p_prog = None
prog = None
p_ctx = None
ctx = None

@ffi.def_extern()
def get_log_level():
    return lib.FLASHROM_MSG_SPEW + 1

@ffi.def_extern()
def py_spi_send_command(writecnt, readcnt, writearr, readarr):
    cocotb.log.info('py_spi_send_command({}, {}, {}, {}) gut: {}'.format(writecnt, readcnt, writearr, readarr, gdut))
    bwritearr = bytes(ffi.buffer(writearr, writecnt))
    cocotb.log.info('py_spi_send_command writearr: {}'.format(bwritearr.hex()))
    breadarr = py_spi_send_command_int(bwritearr, readcnt)
    cocotb.log.info('py_spi_send_command readarr: {}'.format(breadarr.hex()))
    assert(len(breadarr) == readcnt)
    ffi.memmove(readarr, breadarr, readcnt)
    return 0

@cocotb.function
def py_spi_send_command_int(writearr, readcnt):
    cocotb.log.info('py_spi_send_command_int() gut: {}'.format(gdut))
    # readarr = b''
    readarr = bytearray(readcnt)
    yield xfer_begin(gdut)
    for wb in writearr:
        yield xfer_spi(gdut, wb)
    for i in range(readcnt):
        rb = yield xfer_spi(gdut, 0x00)
        # readarr = bytes([rb]))
        readarr[i] = rb
    yield xfer_end(gdut)
    return readarr

lib.set_py_spi_send_command(lib.py_spi_send_command)


assert(lib.flashrom_init(0) == 0)
lib.flashrom_set_log_callback(ffi.addressof(lib, 'do_log_helper'))
p_prog = ffi.new('struct flashrom_programmer **')
nogc.append(p_prog)
cocotb.log.info('p_prog before int: {}'.format(p_prog))
# assert(lib.flashrom_programmer_init(p_prog, ffi.new('char[]', 'dummy'.encode('utf-8')), ffi.new('char[]', 'emulate=MX25L6436'.encode('utf-8'))) == 0)
assert(lib.flashrom_programmer_init(p_prog, ffi.new('char[]', 'cocotb_spi'.encode('utf-8')), ffi.NULL) == 0)
cocotb.log.info('p_prog after int: {}'.format(p_prog))
prog = p_prog[0]
cocotb.log.info('prog: {}'.format(prog))
p_ctx = ffi.new('struct flashrom_flashctx **')
nogc.append(p_ctx)
cocotb.log.info('p_ctx before int: {}'.format(p_ctx))


cocotb.log.info('done setup')

def xbits(n, hi, lo):
    return (n >> lo) & (2**(hi+1 - lo) - 1)

@cocotb.function
def ext_xfer_begin(dut):
    yield xfer_begin(dut)

@cocotb.coroutine
def xfer_begin(dut):
    dut._log.info('-- BEGIN BEGIN')
    yield Timer(clkper, units='ns')
    dut.CS = 0
    dut._log.info('-- BEGIN')
    yield Timer(clkper, units='ns')

@cocotb.function
def ext_xfer_end(dut):
    yield xfer_end(dut)

@cocotb.coroutine
def xfer_end(dut):
    yield Timer(clkper, units='ns')
    dut.CS = 1
    dut._log.info('-- END')
    yield Timer(clkper, units='ns')

@cocotb.function
def ext_xfer_spi(dut, data):
    d = yield xfer_spi(dut, data)
    return d

@cocotb.coroutine
def xfer_spi(dut, data):
    assert(0 <= data <= 0xFF);
    rdata = 0
    for i in range(7, -1, -1):
        dut.SI = (data >> i) & 1
        yield Timer(clkper, units='ns')
        dut.SCLK = 1
        rbit = 0
        if dut.SO.value:
            rbit = 1
        rdata |= (rbit << i)
        yield Timer(clkper, units='ns')
        dut.SCLK = 0
    dut._log.info('--  SPI SDR  {:02x} {:02x}'.format(data, rdata))
    yield Timer(clkper, units='ns')
    return rdata

@cocotb.test()
def test_read(dut):
    global gdut, p_prog, prog, p_ctx, ctx
    gdut = dut
    dut._log.info((cocotb.utils.get_time_from_sim_steps(1, 'ns')))
    dut.SCLK = 0
    dut.CS = 1
    dut.RESET = 0
    yield Timer(2*poweronper, units='ns')
    yield Timer(20*resetper, units='ns')
    dut.RESET = 1
    yield Timer(20*resetper, units='ns')
    yield Timer(20*resetper, units='ns')

    # assert(lib.flashrom_flash_probe(p_ctx, prog, ffi.new('char[]', 'MX25L25635F'.encode('utf-8'))) == 0)
    r = yield cocotb.external(lib.flashrom_flash_probe)(p_ctx, prog, ffi.new('char[]', 'MX25U25635F'.encode('utf-8')))
    cocotb.log.info('flashrom_flash_probe r: {}'.format(r))
    assert(r == 0)
    ctx = p_ctx[0]
    cocotb.log.info('ctx: {}'.format(ctx))
    dut._log.info('getting flash size')
    flash_size = yield cocotb.external(lib.flashrom_flash_getsize(ctx))
    dut._log.info('flash_size: {}'.format(flash_size))
    flash_buf = ffi.new('char[]', flash_size)
    r = yield cocotb.external(lib.flashrom_image_read)(ctx, ffi.addressof(flash_buf), flash_size)
    cocotb.log.info('flashrom_image_read r: {}'.format(r))
    assert(r == 0)
    with open('dump-python.bin', 'wb') as dumpf:
        dumpf.write(ffi.buffer(flash_buf))
    yield cocotb.external(lib.flashrom_flash_release)(ctx)
    r = yield cocotb.external(lib.flashrom_programmer_shutdown(prog))
    cocotb.log.info('flashrom_programmer_shutdown r: {}'.format(r))
    assert(r == 0)
    r = yield cocotb.external(lib.flashrom_shutdown)()
    cocotb.log.info('flashrom_shutdown r: {}'.format(r))
    assert(r == 0)


    dut._log.info("Ok!")
